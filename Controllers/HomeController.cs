using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebLab2.Models;

namespace WebLab2.Controllers {
    public class HomeController : Controller {
        private readonly ILogger<HomeController> _logger;
        private static bool IsAuthorized { get; set; } = false;

        public HomeController(ILogger<HomeController> logger) {
            _logger = logger;
        }

        public IActionResult Index() {
            if (IsAuthorized) {
                return View("Clocks");
            }
            return View("Login");
        }
        public IActionResult Login() {
            return View("Login");
        }
        public IActionResult OnLogin() {
            IsAuthorized = true;
            return RedirectToAction("Index");
        }
        public IActionResult Register() {
            return View("Register");
        }
        public IActionResult About() {
            return View("About");
        }
        public IActionResult Clocks() {
            return View("Clocks");
        }
        public IActionResult Profile() {
            return View("Profile");
        }
        public IActionResult Privacy() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
